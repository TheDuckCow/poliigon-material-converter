# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# Script Name:         Poliigon Material Converter, NodeGroup Exporter
# License:             GPL
# Authors:             Patrick W. Crawford, Google LLC, Poliigon
# Disclaimer:          This is not an official Google Product


import json
import os

import bpy


def color_space_util(node):
	"""Returns the property value based on blender version"""
	if hasattr(node, 'color_space'):
		return node.color_space
	elif node.image and hasattr(node.image, 'colorspace_settings'):
		return node.image.colorspace_settings.name # verify
	return None


def node_group_to_json(ngroup):
	"""Converts a node group into json string format"""

	base = {
		"render_engines":['CYCLES', 'BLENDER_EEVEE'],
		"nodes":{},
		"links":[],
		"defaults":[],
		"inputs":[], # ordered, format of: {name, type}
		"outputs":[], # ordered, format of: {name, type}
		}

	print("Converting "+ngroup.name+" to json")
	for node in ngroup.nodes:
		this_node = {
			"type_id": node.bl_idname,
			"type": node.type,
			"location": [int(coord) for coord in list(node.location)],
			"hide": node.hide,
			"label": node.label
		}

		# extra conditionals based on node type
		if node.type == 'MATH' or node.type == 'VECT_MATH':
			this_node['operation'] = node.operation
		elif node.type == 'BSDF_PRINCIPLED':
			this_node['distribution'] = node.distribution
		elif node.type == 'TEX_IMAGE':
			this_node['color_space'] = color_space_util(node)
		elif node.type == 'MIX_RGB':
			this_node['blend_type'] = node.blend_type
		elif node.type == 'NORMAL_MAP':
			this_node['space'] = node.space
		elif node.type == 'TEX_MUSGRAVE':
			this_node['musgrave_type'] = node.musgrave_type # e.g. 'HETERO_TERRAIN'

		if node.parent:
			this_node['parent'] = node.parent.name # to recosntruct frame parenting

		# assign the ndoe
		base["nodes"][node.name] = this_node

		if node.type == 'REROUTE':
			continue
		if node.type == 'FRAME':
			# mabye set color or text/size etc, or list of belonging nodes
			base["nodes"][node.name]['width'] = node.width
			base["nodes"][node.name]['height'] = node.height
			base["nodes"][node.name]['shrink'] = node.shrink # typically true
			base["nodes"][node.name]['label_size'] = node.label_size
			# ie the textblock it uses if any, perhaps just copy the text w/ delimiting
			if node.text:
				base["nodes"][node.name]['text'] = node.text.as_string()
			continue
		if node.bl_idname == 'NodeSocketShader':
			print('Socket shader: continue (add socket names??)',
				node.name, node.type, node.bl_idname)
			continue
		if node.bl_idname == 'NodeSocketVirtual':
			print('Socket is a virtual socket?',
				node.name, node.type, node.bl_idname)
			continue

		# setting default values
		for socket in node.inputs:
			if not hasattr(socket, "default_value"):
				print("\tSkipping socket, no default_value")
				continue
			if socket.default_value == 0:
				# This specifically addresses compatibility by not including
				# the third value (Value_002) of shader math nodes which are
				# only available in Blender 2.90
				continue
			value = socket.default_value
			if not isinstance(value, (int, str, float, bool)):
				value = list(value)

			default_dict = {
				"node": node.name,
				"socket": socket.name,
				"socket_id": socket.identifier,
				"value": value,
			}
			base['defaults'].append(default_dict)


		# setup input and output order, types, and defaults
		if node.type == 'GROUP_INPUT':
			print('Group input node, generating input fields')
			for i, inp in enumerate(node.outputs):
				print('Group inputs:', i, inp)
				if not inp.name:
					continue # skip empty fields

				value = node.id_data.inputs[i].default_value
				if hasattr(node.id_data.inputs[i], "min_value"):
					minv = node.id_data.inputs[i].min_value
					maxv = node.id_data.inputs[i].max_value
				else:
					minv = None
					maxv = None
				if not isinstance(value, (int, str, float, bool)):
					value = list(value)

				base['inputs'].append(
					{'name': inp.name, 'type': inp.type, 'default': value})
				if minv is not None:
					base['inputs'][-1].update({'min': minv,	'max': maxv})

		if node.type == 'GROUP_OUTPUT':
			print('Group output node, generating output fields')
			for i, out in enumerate(node.inputs):
				print('Group outputs:', i, out)
				if not out.name:
					continue # skip empty fields

				value = node.id_data.outputs[i].default_value
				if hasattr(node.id_data.outputs[i], "min_value"):
					minv = node.id_data.outputs[i].min_value
					maxv = node.id_data.outputs[i].max_value
				else:
					minv = None
					maxv = None
				if not isinstance(value, (int, str, float, bool)):
					value = list(value)

				base['outputs'].append(
					{'name': out.name, 'type': out.type, 'default': value})
				if minv:
					base['outputs'][-1].update({'min': minv, 'max': maxv})

	for link in ngroup.links:
		# Originally used 'socket.name', however is not always unique within
		# not (e.g. both input names = Value), whereas identifier is unique.
		# Should note that script should be ran in Blender 2.8 to ensure
		# identifier name output consistency.

		# Fallback approach, use the path and segment out number value
		# to_path = link.to_socket.path_from_id() # form: nodes["name"].inputs[1]
		# to_index = int(to_path.split('[')[-1][:-1])
		this_link = {
			"from": link.from_node.name,
			"from_socket":link.from_socket.name,
			"from_id":link.from_socket.identifier,
			"to": link.to_node.name,
			"to_socket": link.to_socket.name,
			"to_id": link.to_socket.identifier
		}
		base['links'].append(this_link)

	print("Total for {}: Nodes: {}, links: {}".format(
		ngroup.name, len(base['nodes']), len(base['links'])))

	return json.dumps(base, indent=4, sort_keys=True)


def json_to_file(jstring, file):
	"""Saves the json out to a file next to this blend file"""
	jfile = open(file, 'w')
	jfile.write(jstring)
	jfile.close()


def get_active_node_group(context):
	"""Get the actively selected nodegroup"""

	return None


def save_nodegroup_to_file(ngroup, skip_save=False):
	"""From the given nodegroup datablock, save to json file"""
	path = os.path.join(
		os.path.dirname(bpy.data.filepath),
		ngroup.name+".json")
	if os.path.isfile(path):
		os.remove(path)
	jstring = node_group_to_json(ngroup)
	if skip_save is True:
		print(jstring)
	else:
		print("Saving file: ", path)
		json_to_file(jstring, path)


def save_all_nodegroups(skip_save=False):
	"""Save all nodegroups in the loaded blend to file in json format"""
	if not bpy.data.node_groups:
		print("No node groups in file, aborting")
		return
	for ngroup in bpy.data.node_groups:
		save_nodegroup_to_file(ngroup, skip_save)

if __name__ == "__main__":
	print("Running save_all_nodegroups")
	save_all_nodegroups(skip_save=False)
